

const app = new Vue({
    el: "#app",
    methods: {
        shuffle(a) {
            for (let i = a.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [a[i], a[j]] = [a[j], a[i]];
            }
            return a;
        },
        contestar(respuesta) {

            if (this.preguntaActual.tipo != 3) {
                if (respuesta == this.preguntaActual.is) {
                    this.puntuacion += this.puntosXDificultad;
                    this.historialRespuestas.push(true);
                } else {
                    this.historialRespuestas.push(false);
                }
            } else {

                if (respuesta == this.preguntaActual.partes[this.indexP3].is) {
                    this.puntuacion += this.puntosXDificultad;
                    this.historialRespuestas.push(true);
                } else {
                    this.historialRespuestas.push(false);
                }

                this.indexP3++;
                if (this.indexP3 != this.preguntaActual.partes.length) {
                    this.historialP3.push({
                        cuerpo: this.preguntaActual.partes[this.indexP3 - 1].cuerpo,
                        respuesta: this.preguntaActual.partes[this.indexP3 - 1].opciones[this.preguntaActual.partes[this.indexP3 - 1].is]
                    });
                    this.tiempo = this.tiempoXDificultad;
                    return;
                } else {
                    this.historialP3 = [];
                    this.indexP3 = 0;
                }
            }

            this.index++;
            if (this.index != this.preguntas.length) {
                this.tiempo = this.tiempoXDificultad;
                this.preguntaActual = Object.assign({}, this.preguntas[this.index]);
            } else this.pagina = 3;

        },
        otraVez() {
            this.index = 0;
            this.puntuacion = 0;
            this.pagina = 1;
        },
        cronometro() {
            setInterval(function () {
                if (app.pagina == 2) {
                    app.tiempo--;
                }
                if (app.tiempo == 0 && app.pagina != 3) {
                    app.contestar(null);
                    app.tiempo = app.tiempoXDificultad;
                }
            }, 1000);
        },
        facil() {
            this.historialRespuestas = [];
            this.preguntas = this.shuffle(this.preguntas);
            this.nuevaPuntuacionMax = false;
            this.tiempoXDificultad = 30;
            this.puntosXDificultad = 1;
            this.tiempo = this.tiempoXDificultad;
            this.preguntaActual = Object.assign({}, this.preguntas[this.index]);
            this.pagina = 2;
        },
        normal() {
            this.historialRespuestas = [];
            this.preguntas = this.shuffle(this.preguntas);
            this.nuevaPuntuacionMax = false;
            this.tiempoXDificultad = 10;
            this.puntosXDificultad = 2;
            this.tiempo = this.tiempoXDificultad;
            this.preguntaActual = Object.assign({}, this.preguntas[this.index]);
            this.pagina = 2;
        },
        dificil() {
            this.historialRespuestas = [];
            this.preguntas = this.shuffle(this.preguntas);
            this.nuevaPuntuacionMax = false;
            this.tiempoXDificultad = 5;
            this.puntosXDificultad = 3;
            this.tiempo = this.tiempoXDificultad;
            this.preguntaActual = Object.assign({}, this.preguntas[this.index]);
            this.pagina = 2;
        }
    },
    data: {
        tiempo: 5,
        indexP3: 0,
        historialP3: [],
        nuevaPuntuacionMax: false,
        tiempoXDificultad: 0,
        puntosXDificultad: 0,
        dificultad: 0, //
        historialRespuestas: [],
        pagina: 1,
        puntuacionMaxima: 0,
        puntuacion: 0,
        preguntaActual: {
            tipo: -1
        },
        mensaje: '',
        index: 0,
        preguntas: [{
                 tipo: 1,
                 cuerpo: "2+2 = 1",
                 is: false
             },
             {
                 tipo: 1,
                 cuerpo: "2+1 = 3",
                 is: true
             },
             {
                 tipo: 1,
                 cuerpo: "2+4 = 10",
                 is: false
             },
             {
                 tipo: 1,
                 cuerpo: "2-4 = -2",
                 is: true
             },
             {
                 tipo: 1,
                 cuerpo: "5*4 = 25",
                 is: false
             },
             {
                 tipo: 1,
                 cuerpo: "8-1.5 = 6.5",
                 is: true
             },
             {
                 tipo: 1,
                 cuerpo: "La capital de España es Barcelona",
                 is: false
             },
             {
                 tipo: 1,
                 cuerpo: "Antartida en el país más grande del mundo",
                 is: false
             },
             {
                 tipo: 1,
                 cuerpo: "Luis Miguel nació en México",
                 is: false
             },
             {
                 tipo: 1,
                 cuerpo: "Brasil ganó el mundial de Rusia 2018",
                 is: false
             }
             ,
             {
                 tipo: 1,
                 cuerpo: "1kg de agua es igual a 1 litro de agua",
                 is: true
             },
             {
                 tipo: 1,
                 cuerpo: "Einstein nació en Estados Unidos",
                 is: false
             },
             {
                 tipo: 1,
                 cuerpo: "¿Los pescados nadan?",
                 is: false
             },
             {
                 tipo: 2,
                 cuerpo: '¿Cúal es la capital Nuevo León?',
                 opciones: [
                     'Monterrey',
                     'Toluca',
                     'Campeche'
                 ],
                 is: 0
             },{
                 tipo: 2,
                 cuerpo: 'Es el estado más grande de México',
                 opciones: [
                     'Guadalajara',
                     'Chihuahua',
                     'Baja California Sur'
                 ],
                 is: 1
             },
             {
                 tipo: 2,
                 cuerpo: '¿Cúal es el significado del México?',
                 opciones: [
                     'Tierra del maíz',
                     'Ombligo de la luna',
                     'Lugar de los Dioses',
                     'Ninguna de las anteriores'
                 ],
                 is: 1
             },
             {
                 tipo: 2,
                 cuerpo: '¿Cúal es la raíz cuadrada de 121?',
                 opciones: [
                     '12',
                     '13',
                     '11',
                     'No se'
                 ],
                 is: 2
             },
             {
                 tipo: 2,
                 cuerpo: '¿Cúanto es 8*7?',
                 opciones: [
                     '72',
                     '64',
                     '56',
                     'No se'
                 ],
                 is: 2
             },
             {
                 tipo: 2,
                 cuerpo: '¿Cúanto es 7*8?',
                 opciones: [
                     '72',
                     '64',
                     '66',
                     'Ninguna de las anteriores'
                 ],
                 is: 3
             },{
                 tipo: 2,
                 cuerpo: "En matemáticas ¿Cuánto vale el número <b>e</b>?",
                 opciones: [
                     '2.71',
                     '3.141516',
                     '1.61',
                     'No se'
                 ],
                 is: 0
             },  
             {
                 tipo: 3,
                 partes: [{
                         cuerpo: 'Fue quien descubrió América',
                         opciones: [
                             'Hernan Cortez',
                             'Cuatemoc Blanco',
                             'Bill Gates',
                             'Jesucristo',
                             'Cristobal Colón'
                         ],
                         is: 4
                     },
                     {
                         cuerpo: ' en el año de ',
                         opciones: [
                             '1492',
                             '1552',
                             '1642',
                             '2010'
                         ],
                         is: 0
                     },
                     {
                         cuerpo: ' desembarcó en ',
                         opciones: [
                             'Bahamas',
                             'Cuba',
                             'Toluca'
                         ],
                         is: 1
                     }
                 ]
             }

        ]/*
        
        preguntas: [{
                tipo: 3,
                partes: [{
                        cuerpo: 'Sistemas expertos son llamados así porque ',
                        opciones: [
                            'investiga',
                            'emulan',
                            'copian',
                            'desarrollan'
                        ],
                        is: 1
                    },
                    {
                        cuerpo: ' el ',
                        opciones: [
                            'pensamiento de un experto',
                            'comportamiento de un experto',
                            'razonamiento de un experto'
                        ],
                        is: 2
                    }

                ]
            },
            {
                tipo: 1,
                cuerpo: "Un SE tiene que tener la aptitud de comprender las preguntas del usuario",
                is: true
            },
            {
                tipo: 1,
                cuerpo: "Un SE usa la heurística",
                is: true
            },
            {
                tipo: 1,
                cuerpo: "Un SE accede a una base de datos",
                is: false
            },
            {
                tipo: 1,
                cuerpo: "Un Sistema Tradicional accede a una base de datos",
                is: false
            },
            {
                tipo: 2,
                cuerpo: "Calculas resultados",
                opciones: [
                    'Sistemas Tradicionales',
                    'Sistemas Expertos'
                ],
                is: 0   
            },
            {
                tipo: 2,
                cuerpo: "Dan resultados sin explicaciones",
                opciones: [
                    'Sistemas Expertos',
                    'Sistemas Tradicionales'
                ],
                is: 1
            },
            {
                tipo: 1,
                cuerpo: "Un SE no es costoso de desarrollar o mantener",
                is: false
            },
            {
                tipo: 1,
                cuerpo: "¿Los SEs son confiables?",
                is: true
            },
            {
                tipo: 1,
                cuerpo: "¿Un SE reduce el tiempo para en la toma de decisiones?",
                is: true
            },
            {
                tipo: 3,
                partes: [{
                        cuerpo: 'Con ayuda de los SE, las personas con poca experiencia pueden ',
                        opciones: [
                            'resolver problemas',
                            'comparar situaciones',
                            'ir por la comida'
                        ],
                        is: 0
                    },
                    {
                        cuerpo: ' que requieren de una conocimiento formal ',
                        opciones: [
                            'técnico',
                            'epecializado',
                            'profesional'
                        ],
                        is: 1
                    }

                ]
            },
            {
                tipo: 2,
                cuerpo: "La planificación está compuesto por ",
                opciones: [
                    'Un emulador y un sistema de control',
                    'Un simulador y un sitema de control',
                    'Un emulador y un simulador',
                    'Un sistema de control'
                ],
                is: 1
            },
            {
                tipo: 1,
                cuerpo: "Las redes Bayesianas son métodos de aprendizaje inspirados en la evoluación natural",
                is: false
            },
            {
                tipo: 2,
                cuerpo: " Técnica para tratar el razonamiento con incertidumbre ",
                opciones: [
                    'Redes Bayesianas',
                    'Redes Neuronales',
                    'Algorítmos genéticos',
                    'Aprendizaje'
                ],
                is: 0
            },
            {
                tipo: 2,
                cuerpo: " Obtención de conocimientos en bases de datos ",
                opciones: [
                    'Agentes inteligentes',
                    'Data Mining',
                    'Algorítmos genéticos'
                ],
                is: 1
            },
            {
                tipo: 2,
                cuerpo: " Recuperación de información en Internet ",
                opciones: [
                    'Agentes inteligentes',
                    'Data Mining',
                    'Algorítmos genéticos'
                ],
                is: 0
            }

            
        ]*/

    },
    created: function () {
        this.preguntaActual = Object.assign({}, this.preguntas[0]);
        if (localStorage.puntuacionMaxima) {
            this.puntuacionMaxima = localStorage.puntuacionMaxima;
            //localStorage.puntuacionMaxima = 0;
        } else {
            localStorage.puntuacionMaxima = 0;
            this.puntuacionMaxima = 0;
        };
        this.cronometro();
    },
    computed: {
        revisarPuntuacion() {
            if (this.puntuacion > this.puntuacionMaxima) {
                this.puntuacionMaxima = this.puntuacion;
                localStorage.puntuacionMaxima = this.puntuacion;
                this.mensaje = 'Nueva Puntuación Máxima';
                if (!this.nuevaPuntuacionMax) {
                    this.nuevaPuntuacionMax = true;
                }
                setTimeout(function () {
                    app.mensaje = '';
                }, 1000)
            }
            return '';
        }
    }
})
